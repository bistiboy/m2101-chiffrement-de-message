#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>

// Liste des fonctions
int checkAlphaNum(wchar_t *message);
void replaceAccents(wchar_t *message);
void cryptCesar(wchar_t *message, int code);
void decryptCesar(wchar_t *message, int code);
void cryptVigenere(wchar_t *message, wchar_t *code);
void decryptVigenere(wchar_t *message, wchar_t *code);
void clearBuffer();

// Fonction pour verifier si il y a des caractères spéciaux
int checkAlphaNum(wchar_t *message){

    wchar_t specialCharacters[40]= 
        {'>','<','#','(','{','[',']','}',')','/','*','-','+','.',
        '@','|','&','~','"','_','`','^','?',';',',',':','!','%',
        '$',L'§',L'µ',L'£',L'¤','*',L'€','=',L'°',L'²','\'','\\'};

    for(int i = 0; i < wcslen(message); i++){
        for(int j = 0; j < 40; j++){
            if(message[i] == specialCharacters[j]) return (0);
        }
    }
    return (1);
}

// Fonction pour verifier si il y a des caractères non alphabétique
int checkAlpha(wchar_t *message){
    wchar_t nb[10] = L"0123456789";

    for(int i = 0; i < wcslen(message); i++){
        for(int j = 0; j < 10; j++){
            if(message[i] == nb[j]) return (0);
        }
    }
    return (1);
}

// Fonction pour remplacer les accents
void replaceAccents(wchar_t *message){
    wchar_t accents[54] = L"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñÇç";
    wchar_t whithoutAcc[54] = L"AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc";

    for(int i = 0; i < wcslen(message); i++){
        for(int j = 0; j < 54; j++){
            if(message[i] == accents[j]) message[i] = whithoutAcc[j];
        }
    }
}

// fonction pour chiffrer un message avec l'algorithme Cesar
void cryptCesar(wchar_t *message, int key){
    for(int i = 0; i < wcslen(message); i++){
        if(message[i] >= 97 && message[i] <= 122){
            message[i] = 97 + ((message[i] - 97) + key) % 26;
        } else if(message[i] >= 65 && message[i] <= 90){
            message[i] = 65 + ((message[i] - 65) + key) % 26;
        }
    }
}

// fonction pour déchiffrer un message avec l'algorithme Cesar
void decryptCesar(wchar_t *message, int key){
    for(int i = 0; i < wcslen(message); i++){
        if(message[i] >= 97 && message[i] <= 122){
            message[i] = 97 + ((message[i] - 97) + 26 - key) % 26;
        } else if(message[i] >= 65 && message[i] <= 90){
            message[i] = 65 + ((message[i] - 65) + 26 - key) % 26;
        }
    }
}

// fonction pour chiffrer un message avec l'algorithme Vigénére
void cryptVigenere(wchar_t *message, wchar_t *key){
    wchar_t alphabet[26] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(int i = 0; i < wcslen(message) - 1; i++){
        int correctKey;
        int nbSpaces;

        wchar_t letterCode = key[(i - nbSpaces ) % wcslen(key)];

        for(int j = 0; j < 26; j++){
            if(letterCode == alphabet[j]) correctKey = j;
        }

        if(message[i] >= 97 && message[i] <= 122){
            message[i] = 97 + ((message[i] - 97) + correctKey) % 26;
        } else if(message[i] >= 65 && message[i] <= 90){
            message[i] = 65 + ((message[i] - 65) + correctKey) % 26;
        } else if(message[i] == 32){
            nbSpaces++;
        }
    }
}

// fonction pour déchiffrer un message avec l'algorithme Vigénère
void decryptVigenere(wchar_t *message, wchar_t *key){
    wchar_t alphaMaj[26] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(unsigned i = 0; i < wcslen(message) - 1; i++){
        int correctKey;
        int nbSpaces;

        wchar_t letterCode = key[(i - nbSpaces ) % wcslen(key)];

        for(unsigned j = 0; j < 26; j++){
            if(letterCode == alphaMaj[j]) correctKey = j;
        }

        if(message[i] >= 97 && message[i] <= 122){
            message[i] = 97 + ((message[i] - 97) + 26 - correctKey) % 26;
        } else if(message[i] >= 65 && message[i] <= 90){
            message[i] = 65 + ((message[i] - 65) + 26 - correctKey) % 26;
        } else if(message[i] == 32){
            nbSpaces++;
        }
    }
}

// Fonction pour vider le buffer
void clearBuffer(){
    wchar_t c;
    c = getwchar();
    while(L'\n' != c){
        c = getwchar();
    }
}

// Fonction main
int main()
{
    setlocale(LC_ALL, "");
    wchar_t message[200];

    // Entrée le message a chiffrer/déchiffrer
    printf("Entrer une chaine de caractère (200 max): ");
    fgetws(message, 200, stdin);

    // affiche si le message est incorrecte
    if(checkAlphaNum(message) == 0){
        printf("Message incorrect, recommencer !\n");
        exit(0);
    }

    // Choix de l'algorithme à utilisé
    printf("Choisisez l'algorithme voulu :\n");
    printf("- César ( 1 )\n");
    printf("- Vigenere ( 2 )\n");
    printf("Votre choix : ");

    wchar_t algo = fgetwc(stdin);

    // affiche si le choix est incorrecte
    if(algo != '1' && algo != '2'){
        printf("Algorithme incorrect, recommencer !\n");
        exit(1);
    }

    // Permet de choisir entre chiffrer et déchiffrer
    printf("Choisisez le mode voulu :\n");
    printf("- Chiffrage ( 1 )\n");
    printf("- Déchiffrage ( 2 )\n");
    printf("Votre choix : ");

    clearBuffer();
    wchar_t mode = fgetwc(stdin);

    // Affiche si le choix du mode est incorrecte
    if(mode != '1' && mode != '2'){
        printf("Mode incorrect, recommencer !\n");
        exit(2);
    }

    // Définition des codes pour les différents algorithmes
    if(algo == '1'){

        printf("Choisir votre code : ");

        wchar_t code[10];
        wchar_t *endString;

        clearBuffer();
        fgetws(code, 10, stdin);

        int key = wcstol(code, &endString, 10);

        if(key == 0){
            printf("Code incorrect !\n");
            exit(3);
        }

        (mode == '1') ? cryptCesar(message, key) : decryptCesar(message, key);

        printf("Votre message :\n%ls", message);

    } else if(algo == '2'){
        printf("Veuillez choisir votre clé (10 lettres max) : ");
        wchar_t key[10];

        clearBuffer();
        fgetws(key, 10, stdin);

        // Si message de l'utilisateur incorrect
        if(checkAlphaNum(key) == 0){
            printf("Message incorrect !\n");
        }

        replaceAccents(key);

        for(int i = 0; i < wcslen(key); i++) key[i] = towupper(key[i]);

        (mode == '1') ? cryptVigenere(message, key) : decryptVigenere(message, key);

        printf("Message :\n%ls", message);

    }

    return 0;
}