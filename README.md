# Chiffrement de message

* Thomas LEGRAND
* Antoine BISTI

## Objectifs
Cette application est développée pour permettre de chiffrer ou de déchiffrer un message avec un algorithme César ou Vigenère.

*L'algorithme César : 
Cette algorithme définie chaque lettre de l'alphabet comme un nombre par exemple A=1, B=2, etc.
Ensuite une clé saisie permettra de faire la formule (n-c)mod26.
Par exemple si la lettre est N=14, et la clé est 5, on obtient (14-5)mod26 = 9.
Donc N avec une clé 5 correspond au I avec l'algorithme de César.

*L'algorithme Vigenère:
Dans cette algorithme la clé est un mot, chaque lettre de la clé permettra de trouver le décalage entre celle la 
et la lettre du mot a chiffrer.
Ensuite le décalage utilisée sur les lettres du mot à chiffrer permettra de trouver une autre lettre.
Par exemple si dans le mot on à la lettre C et la lettre du code est B le décalage est de 1, on obtiendra donc la lettre D.


## Les fonctions utilisée

```C
int checkAlphaNum(wchar_t *message)
```
Cette fonction retourne 0 si il y a un cactère spéciale sinon retourne 0.
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer.

```C
int checkAlpha(wchar_t *message)
```
Cette fonction retourne 0 si il y a un caractère non alphabétique sinon retourne 1.
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer.

```C
void replaceAccents(wchar_t *message)
```
Cette fonction remplace les accents présent dans la phrase.
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer.


```C
void cryptCesar(wchar_t *message, int code)
```
Cette fonction chiffre le message en utilisant l'algorithme César.
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer et la clé.


```C
void decryptCesar(wchar_t *message, int code)
```
Cette fonction décode le message en utilisant l'algorithme César
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer et la clé


```C
void cryptVigenere(wchar_t *message, wchar_t *code)
```
Cette fonction permet de chiffrer le message en utilisant l'algorithme Vigenère
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer et la clé


```C
void decryptVigenere(wchar_t *message, wchar_t *code)
```
Cette fonction permet de décoder le message en utilisant l'algorithme Vigenère
* Paramètres entrées, sorties :
    * le message à chiffrer/déchiffrer et la clé
    
# Lien GIT
https://framagit.org/bistiboy/m2101-chiffrement-de-message